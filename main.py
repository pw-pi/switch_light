from RPi import GPIO
from bh1750 import BH1750
from smbus import SMBus
from astral import Location
import datetime
import pytz

#SETUP
LED_RED = 23
LED_GREEN = 24
ADAPTER = 25
SENSITIVITY = 254
BUS_VER = 1
MIN_LX = 17 		# min poziom lx kiedy ma byc wlaczone
ON_STATE_LX = 30 	# max poziom lx gdy swiatlo ma byc wylaczone, jest to tez min lx gdy swiatla sa wlaczone 
TIMEZONE = 'Europe/Warsaw'
CITY = 'Krakow'
REGION = 'Poland'
LAT = 50.06
LONG = 19.945


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(LED_RED, GPIO.OUT)
GPIO.setup(LED_GREEN, GPIO.OUT)
GPIO.setup(ADAPTER, GPIO.OUT)

bus = SMBus(BUS_VER)
sensor = BH1750(bus)
sensor.set_sensitivity(SENSITIVITY)

light_on = False

def is_night(sun, now):
	if now < sun['sunrise'] or now > sun['sunset']:
		return True
	else:
		return False
		
def get_sun():
	l = Location((CITY, REGION, LAT, LONG, TIMEZONE, 0))
	return l.sun();

def turn_on():
	GPIO.output(ADAPTER, GPIO.HIGH)
	GPIO.output(LED_RED, GPIO.LOW)
	GPIO.output(LED_GREEN, GPIO.HIGH)
	
def turn_off():
	GPIO.output(ADAPTER, GPIO.LOW)
	GPIO.output(LED_RED, GPIO.HIGH)
	GPIO.output(LED_GREEN, GPIO.LOW)

def switch_light(night=False):
	global light_on;
	lx = sensor.measure_low_res();
	
	if night is True:
		if light_on is False: 
			if lx >= MIN_LX:
				turn_on()
				light_on = True
		else:
			if lx <= ON_STATE_LX:
				turn_off();
				light_on = False
	else:
		turn_off()
		light_on = False

def main():
	i = 0;
	sun = get_sun();
	while True:
		now = datetime.datetime.now(pytz.timezone(TIMEZONE))
		
		#Sprawdzamy czy dalej ten sam dzien. Jesli nie generujemy godziny sloneczne jeszcze raz
		if now.day != sun['sunset'].day:
			sun = get_sun();
			
		night = is_night(sun, now)
		switch_light(night)
	

if __name__=="__main__":
    main()